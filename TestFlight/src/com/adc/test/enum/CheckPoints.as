package com.adc.test.enum {
	/**
	 * @author Sidney de Koning - Mannetje de Koning { sidney@mannetjedekoning.nl }
	 */
	public class CheckPoints {

		public static const SOME_SECTION_CP : String = "Some Section";
		public static const SOME_SECTION_BUTTON_X_CP : String = "Some Section Button X";
		public static const SOME_SECTION_BUTTON_Y_CP : String = "Some Section Button Y";
		public static const SOME_SECTION_BUTTON_FEEDBACK_CP : String = "Some Section Button Feedback";
	}
}
