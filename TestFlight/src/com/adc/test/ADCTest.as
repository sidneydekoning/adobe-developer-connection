package com.adc.test {

	import com.adc.test.ui.screen.ButtonScreen;
	import com.adobe.ane.testFlight.TestFlight;

	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;

	public class ADCTest extends Sprite {

		public static var FlightData : TestFlight;

		public function ADCTest() {
			/*
			 * Make sure everything aligns correctly
			 */
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;

			NativeApplication.nativeApplication.addEventListener( Event.ACTIVATE, handleActivateApp );
		}

		private function handleActivateApp( event : Event ) : void {
			NativeApplication.nativeApplication.removeEventListener( Event.ACTIVATE, handleActivateApp );

			loadUI();
		}

		private function loadUI() : void {
			var screen : ButtonScreen = new ButtonScreen();
			addChild( screen );
		}
	}
}
