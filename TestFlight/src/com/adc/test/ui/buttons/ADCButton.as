package com.adc.test.ui.buttons {

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;

	/**
	 * @author Sidney de Koning - Mannetje de Koning { sidney@mannetjedekoning.nl }
	 */
	public class ADCButton extends Sprite {

		private var _label : String;
		private var _sbtn : SimpleButton;

		public function ADCButton( label : String ) {
			_label = label;

			var down : Sprite = new Sprite();
			down.graphics.lineStyle( 1, 0x000000 );
			down.graphics.beginFill( 0xCCCCCC );
			down.graphics.drawRect( 0, 0, width, 50 );

			var up : Sprite = new Sprite();
			up.graphics.lineStyle( 1, 0x000000 );
			up.graphics.beginFill( 0xFFCC00 );
			up.graphics.drawRect( 0, 0, width, 50 );

			var over : Sprite = new Sprite();
			over.graphics.lineStyle( 1, 0x000000 );
			over.graphics.beginFill( 0x33CCFF );
			over.graphics.drawRect( 0, 0, width, 50 );

			_sbtn = new SimpleButton();
			_sbtn.upState = up;
			_sbtn.overState = over;
			_sbtn.downState = down;
			_sbtn.hitTestState = up;
			addChild( _sbtn );

			useHandCursor = true;

			addEventListener( Event.ADDED_TO_STAGE, handleAddedToStage );
		}

		override public function get height() : Number {
			return 50;
		}

		override public function get width() : Number {
			return 300;
		}

		private function handleAddedToStage( event : Event ) : void {
			var tf : TextField = new TextField();
			tf.text = _label;

			var tformat : TextFormat = new TextFormat();
			tformat.font = "Arial";
			tformat.size = 20;
			tf.setTextFormat( tformat );

			tf.selectable = false;
			tf.mouseEnabled = false;

			tf.width = tf.textWidth + 5;
			tf.height = tf.textHeight + 10;

			tf.x = _sbtn.x + (_sbtn.width - tf.width) * 0.5;
			tf.y = _sbtn.y + (_sbtn.height - tf.height) * 0.5;

			addChild( tf );
		}
	}
}
