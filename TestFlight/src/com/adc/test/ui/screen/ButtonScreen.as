package com.adc.test.ui.screen {

	import com.adc.test.enum.CheckPoints;
	import com.adc.test.ui.buttons.ADCButton;
	import com.adobe.ane.testFlight.TestFlight;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author Sidney de Koning - Mannetje de Koning { sidney@mannetjedekoning.nl }
	 */
	public class ButtonScreen extends Sprite {

		private var _tf : TestFlight;

		public function ButtonScreen() {
			addEventListener( Event.ADDED_TO_STAGE, build );
		}

		private function build( event : Event ) : void {
			var i : int = 0;
			var offset : int = 100;

			var btn : ADCButton = new ADCButton( "Send log message" );
			btn.addEventListener( MouseEvent.CLICK, handleSendLog );
			btn.x = 0 + (stage.fullScreenWidth - btn.width) * 0.5;
			btn.y = btn.y + btn.height + (i++ * offset);
			addChild( btn );

			btn = new ADCButton( "Open feedback view" );
			btn.addEventListener( MouseEvent.CLICK, handleOpenFeedBackView );
			btn.x = 0 + (stage.fullScreenWidth - btn.width) * 0.5;
			btn.y = btn.y + btn.height + (i++ * offset);
			addChild( btn );

			btn = new ADCButton( "Pass checkpoint" );
			btn.addEventListener( MouseEvent.CLICK, handlePassCheckPoint );
			btn.x = 0 + (stage.fullScreenWidth - btn.width) * 0.5;
			btn.y = btn.y + btn.height + (i++ * offset);
			addChild( btn );

			btn = new ADCButton( "Submit custom feedback" );
			btn.addEventListener( MouseEvent.CLICK, handleSubmitCustomFeedBack );
			btn.x = 0 + (stage.fullScreenWidth - btn.width) * 0.5;
			btn.y = btn.y + btn.height + (i++ * offset);
			addChild( btn );

			if (TestFlight.isSupported) {
				_tf = new TestFlight( "HERE_GOES_YOUR_TOKEN", true );

				var stderr : Object = new Object();
				stderr.key = "logToSTDERR";
				stderr.value = "YES";

				var console : Object = new Object();
				console.key = "logToConsole";
				console.value = "YES";

				var optArr : Array = new Array();
				optArr[0] = stderr;
				optArr[1] = console;

				_tf.setOptions( optArr );

				_tf.passCheckPoint( CheckPoints.SOME_SECTION_CP );
			}
		}

		private function handleOpenFeedBackView( event : MouseEvent ) : void {
			if (TestFlight.isSupported) {
				_tf.passCheckPoint( CheckPoints.SOME_SECTION_BUTTON_FEEDBACK_CP );
				_tf.openFeedBackView();
			}
		}

		private function handlePassCheckPoint( event : MouseEvent ) : void {
			if (TestFlight.isSupported) {
				_tf.passCheckPoint( CheckPoints.SOME_SECTION_BUTTON_X_CP );
			}
		}

		private function handleSubmitCustomFeedBack( event : MouseEvent ) : void {
			if (TestFlight.isSupported) {
				_tf.passCheckPoint( CheckPoints.SOME_SECTION_BUTTON_Y_CP );
				_tf.submitCustomFeedBack( "Here is my custom feedback for the app, this shoudl come from custom UI / textfield" );
			}
		}

		private function handleSendLog( event : MouseEvent ) : void {
			if (TestFlight.isSupported) {
				_tf.log( "Sending log message to TestFlight" );
			}
		}
	}
}
